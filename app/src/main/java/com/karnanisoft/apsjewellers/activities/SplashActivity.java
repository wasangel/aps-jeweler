package com.karnanisoft.apsjewellers.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.karnanisoft.apsjewellers.MainActivity;
import com.karnanisoft.apsjewellers.R;

public class SplashActivity extends BaseActivity {

    private static final long SPLASH_TIME_OUT = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);


//        FirebaseApp.initializeApp(this);
//        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
//            if (task.isSuccessful()) {
//
//                String fmToken = task.getResult().getToken();
//                SharedPreferences.Editor editor = getSharedPreferences(WebService.SP_FCM_MAIN, MODE_PRIVATE).edit();
//                editor.putString(WebService.FCM_TOKEN, "");
//                editor.apply();
//                Log.d("FCM_TOKEN", "onComplete: " + fmToken);
//                //Toast.makeText(activity, fmToken, Toast.LENGTH_SHORT).show();
//            }
//        });

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();
            }
        }, SPLASH_TIME_OUT);
    }


}