package com.karnanisoft.apsjewellers.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.karnanisoft.apsjewellers.R;
import com.karnanisoft.apsjewellers.datamodels.authmodels.OtpResponseModel;
import com.karnanisoft.apsjewellers.utils.AppController;
import com.karnanisoft.apsjewellers.utils.ConstantFunctions;
import com.karnanisoft.apsjewellers.utils.IntentData;
import com.karnanisoft.apsjewellers.utils.SessionManager;
import com.karnanisoft.apsjewellers.utils.WebService;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.etxRegName)
    AppCompatEditText etxRegName;
    @BindView(R.id.etxRegEmail)
    AppCompatEditText etxRegEmail;
    @BindView(R.id.etxRegMobile)
    AppCompatEditText etxRegMobile;
    @BindView(R.id.etxRegPass)
    AppCompatEditText etxRegPass;
    @BindView(R.id.etxRegConfirmPass)
    AppCompatEditText etxRegConfirmPass;
    @BindView(R.id.btnRegSignUp)
    Button btnRegSignUp;
    @BindView(R.id.txtSignIn)
    TextView txtSignIn;
    @BindView(R.id.layoutRegister)
    LinearLayout layoutRegister;
    private Activity activity = RegisterActivity.this;
    private String userName, userEmail, userMobile, userPass, userCpass, fcmToken = "", fmToken;


    private SessionManager mySessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);

        setTitle("");
        FirebaseApp.initializeApp(this);
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (task.isSuccessful()) {

                    fmToken = task.getResult().getToken();
                    Log.d("FCM_TOKEN", "onComplete: " + fmToken);
                    SharedPreferences.Editor editor = getSharedPreferences(WebService.SP_FCM_MAIN, MODE_PRIVATE).edit();
                    editor.putString(WebService.FCM_TOKEN, fmToken);
                    editor.apply();
                    SharedPreferences preferences = getSharedPreferences(WebService.SP_FCM_MAIN, Context.MODE_PRIVATE);
                    fcmToken = preferences.getString(WebService.FCM_TOKEN, fmToken);
                    //Toast.makeText(activity, fmToken, Toast.LENGTH_SHORT).show();
                }
            }
        });
        mySessionManager = new SessionManager(activity);

        txtSignIn.setPaintFlags(txtSignIn.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Animation slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        Animation slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);

        if (layoutRegister.getVisibility() == View.GONE) {

            layoutRegister.startAnimation(slideUp);
            layoutRegister.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnClick({R.id.btnRegSignUp, R.id.txtSignIn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnRegSignUp:

                userName = Objects.requireNonNull(etxRegName.getText()).toString().trim();
                userEmail = Objects.requireNonNull(etxRegEmail.getText()).toString().trim();
                userMobile = Objects.requireNonNull(etxRegMobile.getText()).toString().trim();
                userPass = Objects.requireNonNull(etxRegPass.getText()).toString().trim();
                userCpass = Objects.requireNonNull(etxRegConfirmPass.getText()).toString().trim();
                if (onCheckInput()) {
                    onSendOtp();
                }
                break;
            case R.id.txtSignIn:
                startActivity(new Intent(activity, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY));
                finish();
                break;
        }
    }

    private void onSendOtp() {
        final Dialog dialog = ConstantFunctions.onShowProgressDialog(activity);
        StringRequest otpRequest = new StringRequest(Request.Method.POST, WebService.API_SEND_OTP,
                response -> {
                    dialog.dismiss();
                    onHandleResponse(response);
                }, error -> {

        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(WebService.PARAM_KEY_EMAIL, userEmail);
                params.put(WebService.PARAM_KEY_MOBILE, userMobile);
                params.put(WebService.PARAM_KEY_USER_NAME, userName);
                params.put(WebService.PARAM_KEY_PASSWORD, userPass);
                params.put(WebService.PARAM_KEY_C_PASSWORD, userCpass);
                return params;

            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<>();
                param.put("APIKEY", WebService.API_KEY);
                return param;
            }

        };
        AppController.getInstance().addToRequestQueue(otpRequest);
    }

    private void onHandleResponse(String response) {
        Log.d("LOGIN_RESPONSE", "onHandleResponse: " + response);
        OtpResponseModel otpResponseModel = new Gson().fromJson(response, OtpResponseModel.class);
        if (otpResponseModel.getCode()) {
            mySessionManager.saveUser(userName, userPass, userEmail, userMobile);
            startActivity(new Intent(activity, OTPVerify.class).putExtra(IntentData.KEY_VERIFY_OTP_SOURCE, IntentData.VERIFY_OTP_REGISTRATION));
        } else {
            ConstantFunctions.OnShowAlertMessage(activity, otpResponseModel.getMessage());
        }
    }

    private boolean onCheckInput() {

        if (TextUtils.isEmpty(userName)) {
            ConstantFunctions.OnShowAlertMessage(activity, "Enter your Name");
            return false;
        } else if (TextUtils.isEmpty(userEmail) || !Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()) {
            ConstantFunctions.OnShowAlertMessage(activity, "Please Enter Valid Email Id");
            return false;
        } else if (TextUtils.isEmpty(userMobile) || !userMobile.matches(ConstantFunctions.INDIAN_MOBILE_NUMBER_PATTERN)) {
            ConstantFunctions.OnShowAlertMessage(activity, "Enter Valid Mobile Number");
            return false;
        } else if (TextUtils.isEmpty(userPass)) {
            ConstantFunctions.OnShowAlertMessage(activity, "Enter Your Password");

            return false;
        } else if (userPass.length() <= 6) {
            ConstantFunctions.OnShowAlertMessage(activity, "Your password is weak. Please Enter minimum 6 digit character password ");
            return false;
        } else if (TextUtils.isEmpty(userCpass)) {
            ConstantFunctions.OnShowAlertMessage(activity, "Enter Your Confirm Password");
            return false;
        } else if (!userCpass.equals(userPass)) {
            ConstantFunctions.OnShowAlertMessage(activity, "Passwords are Mismatch");
            return false;
        }
        return true;
    }


}