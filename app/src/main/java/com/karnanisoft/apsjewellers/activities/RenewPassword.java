package com.karnanisoft.apsjewellers.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.karnanisoft.apsjewellers.R;
import com.karnanisoft.apsjewellers.datamodels.authmodels.OtpResponseModel;
import com.karnanisoft.apsjewellers.utils.AppController;
import com.karnanisoft.apsjewellers.utils.ConstantFunctions;
import com.karnanisoft.apsjewellers.utils.IntentData;
import com.karnanisoft.apsjewellers.utils.WebService;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RenewPassword extends AppCompatActivity {

    @BindView(R.id.etxRPassword)
    AppCompatEditText etxRPassword;
    @BindView(R.id.etxRCPassword)
    AppCompatEditText etxRCPassword;
    @BindView(R.id.btnChangePass)
    Button btnChangePass;
    @BindView(R.id.layoutResetPass)
    LinearLayout layoutResetPass;
    private String password, cpassword, userOtp, mobileNumber;
    private Activity activity = RenewPassword.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_renew_password);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);

        setTitle("");
        Animation slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        Animation slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);


        mobileNumber = getIntent().getStringExtra(IntentData.KEY_MOBILE_NUMBER);
        userOtp = getIntent().getStringExtra(IntentData.KEY_VERIFY_OTP);

        if (layoutResetPass.getVisibility() == View.GONE) {

            layoutResetPass.startAnimation(slideUp);
            layoutResetPass.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @OnClick(R.id.btnChangePass)
    public void onViewClicked() {
        password = Objects.requireNonNull(etxRPassword.getText()).toString().trim();
        cpassword = Objects.requireNonNull(etxRCPassword.getText()).toString().trim();
        if (onCheckInput()) {

            // Toast.makeText(activity, mobileNumber, Toast.LENGTH_SHORT).show();
            onRenewPassword();
        }
    }


    private void onRenewPassword() {

        Dialog dialog = ConstantFunctions.onShowProgressDialog(activity);
        StringRequest renewPasswordRequest = new StringRequest(Request.Method.POST, WebService.API_RESET_PASSWORD,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.cancel();
                        onHandleResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(WebService.PARAM_KEY_PASSWORD, password);
                params.put(WebService.PARAM_KEY_OTP, userOtp);
                params.put(WebService.PARAM_KEY_MOBILE, mobileNumber);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<>();
                param.put("APIKEY", WebService.API_KEY);
                return param;
            }

        };
        AppController.getInstance().addToRequestQueue(renewPasswordRequest);
    }

    private void onHandleResponse(String response) {
        OtpResponseModel responseModel = new Gson().fromJson(response, OtpResponseModel.class);
        if (responseModel.getCode()) {
            Toast.makeText(activity, responseModel.getMessage(), Toast.LENGTH_LONG).show();
            startActivity(new Intent(activity, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY));
            finish();
        } else {
            ConstantFunctions.OnShowAlertMessage(activity, responseModel.getMessage());
        }
    }


    private boolean onCheckInput() {

        if (TextUtils.isEmpty(password)) {
            ConstantFunctions.OnShowAlertMessage(activity, "Enter Your Password");
            return false;
        } else if (password.length() <= 6) {
            ConstantFunctions.OnShowAlertMessage(activity, "Your password is weak. Please Enter minimum 6 digit character password ");
            return false;
        } else if (TextUtils.isEmpty(cpassword)) {
            ConstantFunctions.OnShowAlertMessage(activity, "Enter Your Confirm Password");
            return false;
        } else if (!password.equals(cpassword)) {
            ConstantFunctions.OnShowAlertMessage(activity, "Passwords are Mismatch");
            return false;
        }
        return true;
    }

}