package com.karnanisoft.apsjewellers.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.chaos.view.PinView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.karnanisoft.apsjewellers.MainActivity;
import com.karnanisoft.apsjewellers.R;
import com.karnanisoft.apsjewellers.datamodels.authmodels.OtpResponseModel;
import com.karnanisoft.apsjewellers.utils.AppController;
import com.karnanisoft.apsjewellers.utils.ConstantFunctions;
import com.karnanisoft.apsjewellers.utils.IntentData;
import com.karnanisoft.apsjewellers.utils.SessionManager;
import com.karnanisoft.apsjewellers.utils.WebService;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OTPVerify extends AppCompatActivity {

    @BindView(R.id.firstPinView)
    PinView firstPinView;
    @BindView(R.id.txtResendTimer)
    TextView txtResendTimer;
    @BindView(R.id.btnVerify)
    Button btnVerify;
    private SessionManager mySessionManager;
    private Activity activity = OTPVerify.this;
    private long maxTimeInMilliseconds = 30000;// in your case
    private String enteredOtp;
    private Integer otpActual;
    private String fcmToken = "", fmToken;
    private String username, userpass, userEmail, userMobile;
    private int verifyOtpCase;
    private String mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_o_t_p_verify);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);

        setTitle("Verify OTP");

        FirebaseApp.initializeApp(this);
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (task.isSuccessful()) {

                    fmToken = task.getResult().getToken();
                    Log.d("FCM_TOKEN", "onComplete: " + fmToken);
                    SharedPreferences.Editor editor = getSharedPreferences(WebService.SP_FCM_MAIN, MODE_PRIVATE).edit();
                    editor.putString(WebService.FCM_TOKEN, fmToken);
                    editor.apply();
                    SharedPreferences preferences = getSharedPreferences(WebService.SP_FCM_MAIN, Context.MODE_PRIVATE);
                    fcmToken = preferences.getString(WebService.FCM_TOKEN, fmToken);
                    //Toast.makeText(activity, fmToken, Toast.LENGTH_SHORT).show();
                }
            }
        });

        verifyOtpCase = getIntent().getIntExtra(IntentData.KEY_VERIFY_OTP_SOURCE, 0);
        if (verifyOtpCase == IntentData.VERIFY_OTP_REGISTRATION) {
            mySessionManager = new SessionManager(activity);
            username = mySessionManager.getSpUserName();
            userpass = mySessionManager.getSpUserPassword();
            userEmail = mySessionManager.getSpUserEmail();
            userMobile = mySessionManager.getSpUserMobile();
        } else {
            userMobile = getIntent().getStringExtra(IntentData.KEY_MOBILE_NUMBER);
        }
        startTimer(maxTimeInMilliseconds, 1000);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnClick({R.id.txtResendTimer, R.id.btnVerify})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtResendTimer:
                break;
            case R.id.btnVerify:
                enteredOtp = firstPinView.getText().toString().trim();
                onVerifyOtp();
                break;
        }
    }

    public void startTimer(final long finish, long tick) {
        CountDownTimer t;
        t = new CountDownTimer(finish, tick) {

            public void onTick(long millisUntilFinished) {
                long remainedSecs = millisUntilFinished / 1000;
                txtResendTimer.setText("Resend in  " + (remainedSecs / 60) + ":" + (remainedSecs % 60));// manage it accordign to you
            }

            public void onFinish() {
                txtResendTimer.setText("Resend ?");
                txtResendTimer.setClickable(true);
                txtResendTimer.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_resend, 0);
                // progressDialog.dismiss();
                //  Toast.makeText(activity, "Finish", Toast.LENGTH_SHORT).show();

                cancel();
            }
        }.start();
    }


    private void onVerifyOtp() {

        final Dialog dialog = ConstantFunctions.onShowProgressDialog(activity);
        StringRequest otpVerifyRequest = new StringRequest(Request.Method.POST, WebService.API_VERIFY_OTP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        onHandleResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("", "onErrorResponse: " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(WebService.PARAM_KEY_OTP_MOBILE, userMobile);
                params.put(WebService.PARAM_KEY_OTP, enteredOtp);
                params.put(WebService.PARAM_KEY_FCM_TOKEN, fcmToken);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<>();
                param.put("APIKEY", WebService.API_KEY);
                return param;
            }

        };
        AppController.getInstance().addToRequestQueue(otpVerifyRequest);
    }

    private void onHandleResponse(String response) {
        OtpResponseModel responseModel = new Gson().fromJson(response, OtpResponseModel.class);
        if (responseModel.getCode()) {

            if (verifyOtpCase == IntentData.VERIFY_OTP_REGISTRATION) {
                startActivity(new Intent(activity, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
            } else {
                startActivity(new Intent(activity, RenewPassword.class)
                        .putExtra(IntentData.KEY_MOBILE_NUMBER, userMobile)
                        .putExtra(IntentData.KEY_VERIFY_OTP, enteredOtp)
                );
            }
            Toast.makeText(activity, responseModel.getMessage(), Toast.LENGTH_SHORT).show();
        } else {
            ConstantFunctions.OnShowAlertMessage(activity, responseModel.getMessage());
        }
    }
}