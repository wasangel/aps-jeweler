package com.karnanisoft.apsjewellers.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.karnanisoft.apsjewellers.R;
import com.karnanisoft.apsjewellers.datamodels.authmodels.OtpResponseModel;
import com.karnanisoft.apsjewellers.utils.AppController;
import com.karnanisoft.apsjewellers.utils.ConstantFunctions;
import com.karnanisoft.apsjewellers.utils.IntentData;
import com.karnanisoft.apsjewellers.utils.WebService;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPassword extends AppCompatActivity {

    @BindView(R.id.etxFMobile)
    AppCompatEditText etxFMobile;
    @BindView(R.id.btnResetPass)
    Button btnResetPass;
    @BindView(R.id.layoutResetPass)
    LinearLayout layoutResetPass;
    @BindView(R.id.etxFEmail)
    AppCompatEditText etxFEmail;

    private String mobileNumber, email;
    private Activity activity = ForgotPassword.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);

        setTitle("");


        Animation slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        Animation slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);

        if (layoutResetPass.getVisibility() == View.GONE) {

            layoutResetPass.startAnimation(slideUp);
            layoutResetPass.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btnResetPass)
    public void onViewClicked() {
        mobileNumber = Objects.requireNonNull(etxFMobile.getText()).toString().trim();
        email = Objects.requireNonNull(etxFEmail.getText()).toString().trim();
        if (mobileNumber.length() == 10) {
            onSendOtp();
        } else {
            ConstantFunctions.OnShowAlertMessage(activity, "Please enter valid mobile number");
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void onSendOtp() {
        final Dialog dialog = ConstantFunctions.onShowProgressDialog(activity);
        StringRequest otpRequest = new StringRequest(Request.Method.POST, WebService.API_FORGOT_OTP,
                response -> {
                    dialog.dismiss();
                    onHandleResponse(response);
                }, error -> {
            Toast.makeText(activity, error.toString(), Toast.LENGTH_SHORT).show();
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(WebService.PARAM_KEY_EMAIL, email);
                params.put(WebService.PARAM_KEY_MOBILE, mobileNumber);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<>();
                param.put("APIKEY", WebService.API_KEY);
                return param;
            }

        };
        AppController.getInstance().addToRequestQueue(otpRequest);
    }

    private void onHandleResponse(String response) {
        Log.d("LOGIN_RESPONSE", "onHandleResponse: " + response);
        OtpResponseModel otpResponseModel = new Gson().fromJson(response, OtpResponseModel.class);
        if (otpResponseModel.getCode()) {
            startActivity(new Intent(activity, OTPVerify.class)
                    .putExtra(IntentData.KEY_VERIFY_OTP_SOURCE, IntentData.VERIFY_OTP_FORGOT_PASSWORD)
                    .putExtra(IntentData.KEY_MOBILE_NUMBER, mobileNumber)
            );
        } else {
            ConstantFunctions.OnShowAlertMessage(activity, otpResponseModel.getMessage());
        }
    }


}