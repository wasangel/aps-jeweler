package com.karnanisoft.apsjewellers.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.karnanisoft.apsjewellers.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;



public class MyFirebaseMessagingService extends FirebaseMessagingService {


    public static final String TAG = "FCM_MAIN_HERE";

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token);
    }

    private void sendRegistrationToServer(String token) {

        Log.d(TAG, "sendRegistrationToServer: " + token);
        SharedPreferences.Editor editor = getSharedPreferences(WebService.SP_FCM_MAIN, MODE_PRIVATE).edit();
        editor.putString(WebService.FCM_TOKEN, token);
        editor.apply();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.


        //  Toast.makeText(this, remoteMessage.getData().toString(), Toast.LENGTH_SHORT).show();
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data: " + remoteMessage.getData().get("body"));

            handleNow(remoteMessage.getData().get("body"));
//            if (/* Check if data needs to be processed by long running job */ true) {
//                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
//                //scheduleJob();
//            } else {
//                // Handle message within 10 seconds
//                handleNow(remoteMessage);
//            }

        }

        // Check if message contains a notification payload.
//        if (remoteMessage.getNotification() != null) {
//            Log.d("WASIMS", "onMessageReceived: " + remoteMessage.toString());
//            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
//            Log.d(TAG, "Message Notification Title: " + remoteMessage.getNotification().getTitle());
//
//            handleNow(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
//        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void handleNow(String body) {
        JSONObject json = null;
        try {
            json = new JSONObject(body);
            String imgUrl = json.getString("imgurl");
            int code = json.getInt("code");
            String message = json.getString("message");
            String title = json.getString("title");
            onShowNotification(title, message, imgUrl, code);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void onShowNotification(String title, final String message, String imgurl, int code) {
        Random rand = new Random();
//
//        Log.e(TAG, "title: I AM HERE" + title);
        final int value = rand.nextInt(11221);
//

        final NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "my_channel_id_01";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_DEFAULT);

            // Configure the notification channel.
            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }


        final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);


        Log.d(TAG, "onShowNotification: Code - " + code);
        PendingIntent pendingIntent = null;
//        switch (code) {
//
//            case WebService.NOTIFICATION_GENERAL:
//                Intent intentNormal = new Intent(getApplicationContext(), NotificationsActivity.class);
//
//                pendingIntent = PendingIntent.getActivity(this, 0, intentNormal, PendingIntent.FLAG_CANCEL_CURRENT);
//
//                if (!imgurl.equals("NA")) {
//
//                    notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle()
//                            .bigLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo))
//                            .setBigContentTitle(message)
//                            .bigPicture(getBitmapFromUrl(imgurl))
//                    );
//                }
//                notificationBuilder.setAutoCancel(true)
//                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo))
//                        .setDefaults(Notification.DEFAULT_ALL)
//                        .setWhen(System.currentTimeMillis())
//                        .setSmallIcon(R.drawable.logo)
//                        .setContentIntent(pendingIntent)
//                        .setTicker(getString(R.string.app_name))
//                        .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
//                        .setPriority(Notification.PRIORITY_MAX)
//                        .setContentTitle(title)
//                        .setContentText(message)
//                        .setContentInfo(getString(R.string.app_name));
//                notificationManager.notify(/*notification id*/value, notificationBuilder.build());
//                break;
//            case WebService.NOTIFICATION_OFFER:
//
//                Log.d(TAG, "onShowNotification: " + imgurl);
//                if (!imgurl.equals("NA")) {
//
//                    notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle()
//                            .bigLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo))
//                            .setBigContentTitle(message)
//                            .bigPicture(getBitmapFromUrl(imgurl))
//                    );
//                }
//
//                Intent intent = new Intent(getApplicationContext(), CouponCodeActivity.class);
//
//                pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
//
//                notificationBuilder.setAutoCancel(true)
//                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo))
//                        .setDefaults(Notification.DEFAULT_ALL)
//                        .setWhen(System.currentTimeMillis())
//                        .setSmallIcon(R.drawable.logo)
//                        .setContentIntent(pendingIntent)
//                        .setTicker(getString(R.string.app_name))
//                        .setContentText(message)
//                        .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
//                        .setPriority(Notification.PRIORITY_MAX)
//                        .setContentTitle(title)
//                        .setContentInfo(getString(R.string.app_name));
//                notificationManager.notify(/*notification id*/value, notificationBuilder.build());
//
//                break;
//
//            case WebService.NOTIFICATION_ORDER:
//                String orderId = title.substring(title.indexOf("#") + 1, title.indexOf("-"));
//
//                Intent intentMyOrders = new Intent(getApplicationContext(), DetailMyOrderActivity.class).putExtra(IntentData.KEY_MAIN_ORDER_ID, orderId);
//
//                pendingIntent = PendingIntent.getActivity(this, 0, intentMyOrders, PendingIntent.FLAG_CANCEL_CURRENT);
//
//                notificationBuilder.setAutoCancel(true)
//                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo))
//                        .setDefaults(Notification.DEFAULT_ALL)
//                        .setWhen(System.currentTimeMillis())
//                        .setSmallIcon(R.drawable.logo)
//                        .setContentIntent(pendingIntent)
//                        .setTicker(getString(R.string.app_name))
//                        .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
//                        .setPriority(Notification.PRIORITY_MAX)
//                        .setContentTitle(title)
//                        .setContentText(message)
//                        .setContentInfo(getString(R.string.app_name));
//                notificationManager.notify(/*notification id*/value, notificationBuilder.build());
//                break;
//
//            case WebService.NOTIFICATION_ISSUE:
//            case WebService.NOTIFICATION_PAYMENT:
//                Intent intentIssue = new Intent(getApplicationContext(), NotificationsActivity.class);
//
//                pendingIntent = PendingIntent.getActivity(this, 0, intentIssue, PendingIntent.FLAG_CANCEL_CURRENT);
//
//                notificationBuilder.setAutoCancel(true)
//                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo))
//                        .setDefaults(Notification.DEFAULT_ALL)
//                        .setWhen(System.currentTimeMillis())
//                        .setSmallIcon(R.drawable.logo)
//                        .setContentIntent(pendingIntent)
//                        .setTicker(getString(R.string.app_name))
//                        .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
//                        .setPriority(Notification.PRIORITY_MAX)
//                        .setContentTitle(title)
//                        .setContentText(message)
//                        .setContentInfo(getString(R.string.app_name));
//                notificationManager.notify(/*notification id*/value, notificationBuilder.build());
//                break;
//
//
//            default:
//
//
//        }


    }

    public Bitmap getBitmapFromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }


}
