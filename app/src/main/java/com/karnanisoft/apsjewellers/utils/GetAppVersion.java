package com.karnanisoft.apsjewellers.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.karnanisoft.apsjewellers.BuildConfig;

import org.jsoup.Jsoup;


public class GetAppVersion extends AsyncTask<String, Void, String> {

    private Context context;
    private AlertDialog alertDialog;
    private String currentVersion;

    public GetAppVersion(Context context) {
        this.context = context;

    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        currentVersion = BuildConfig.VERSION_NAME;
    }

    @Override
    protected String doInBackground(String... strings) {
        String newVersion = null;
        try {
            newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" +
                    BuildConfig.APPLICATION_ID)
                    .timeout(10000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get()
                    .select("div:contains(Current Version)").last().parent()
                    .select("span").last()
                    .ownText();
            return newVersion;
        } catch (Exception e) {
            return newVersion;
        }
    }

    @Override
    protected void onPostExecute(String onlineVersion) {
        super.onPostExecute(onlineVersion);
        if (onlineVersion != null && !onlineVersion.isEmpty()) {
            if (value(currentVersion) < value(onlineVersion)) {
                //System.out.println("currentVersion  " + currentVersion + "onlineVersion   " + onlineVersion);
                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();
                    alertDialog = null;

                }
                alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle("Update available");
                alertDialog.setMessage("New version available on Play Store");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "&hl=en"));
                                //alertDialog.dismiss();
                                context.startActivity(i);
                            }
                        });
                alertDialog.setCancelable(false);
                alertDialog.show();
            }
        }
        Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);
    }

    private long value(String string) {
        string = string.trim();
        if (string.contains(".")) {
            final int index = string.lastIndexOf(".");
            return value(string.substring(0, index)) * 100 + value(string.substring(index + 1));
        } else {
            return Long.valueOf(string);
        }
    }
}