package com.karnanisoft.apsjewellers.utils;


//import in.co.rkdigi.user.BuildConfig;

import com.karnanisoft.apsjewellers.BuildConfig;

public class WebService {
    public static final String PAYEE_ADDRESS = "";
    //    public static final String PAYEE_ADDRESS = "7770062552@upi";
    public static final String API_KEY = "UHT7hjquoUba8mpEeFUyWY1ilGcZhlkP";
    public static final String PLAY_STORE_APP_LINK = "https://play.google.com/store/apps/details?id=" +
            BuildConfig.APPLICATION_ID;
    public static final String API_BASE_URL = "http://apsjewellers.com/admin/restapp/api/";
    public static final String IMAGE_BASE_URL = "";
    public static final String API_LOGIN = API_BASE_URL + "login";
    public static final String API_REGISTER = API_BASE_URL + "newregister";
    public static final String API_SEND_OTP = API_BASE_URL + "sendotp";
    public static final String API_FORGOT_OTP = API_BASE_URL + "forgotpassword";
    public static final String API_VERIFY_OTP = API_BASE_URL + "verifyotp";
    public static final String API_RESET_PASSWORD = API_BASE_URL + "resetpass";
    public static final String API_DASHBOARD = API_BASE_URL + "newdashboard";
    public static final String API_CART = API_BASE_URL + "mycart";


    public static final String PARAM_KEY_PASSWORD = "password";
    public static final String PARAM_KEY_C_PASSWORD = "c_password";
    public static final String PARAM_KEY_OTP = "user_otp";
    public static final String PARAM_KEY_FCM_TOKEN = "fcm_token";
    public static final String PARAM_KEY_USER_NAME = "customer_name";
    public static final String PARAM_KEY_EMAIL = "email";
    public static final String PARAM_KEY_MOBILE = "mobile";
    public static final String PARAM_KEY_OTP_MOBILE = "otp_mobile";


    public static final String API_BASE_URL_POST = API_BASE_URL + "master_post.php";
    public static final String API_BASE_URL_GET = API_BASE_URL + "master_get.php?";

    public static final String PARAM_NAME = "name";
    public static final String PARAM_KEY_ACTION = "action";

    public static final String PARAM_KEY_USER_PASS = "user_pass";


    public static final String OS_ORDER_PLACED = "1";
    public static final String OS_ORDER_CONFIRMED = "2";
    public static final String OS_ORDER_DISPATCHED = "3";
    public static final String OS_ORDER_DELIVERED = "4";
    public static final String OS_ORDER_CANCELED = "5";
    public static final String OS_ORDER_FAILED = "6";
    public static final String OS_ORDER_CANCELED_REQUEST = "7";


    public static final String PARAM_KEY_ADDRESS_ID = "address_id";
    public static final String SP_FCM_MAIN = "SP_FCM_MAIN";
    public static final String FCM_TOKEN = "FCM_TOKEN";

    public static final int NOTIFICATION_GENERAL = 1;
    public static final int NOTIFICATION_OFFER = 2;
    public static final int NOTIFICATION_ORDER = 3;
    public static final int NOTIFICATION_ISSUE = 4;
    public static final int NOTIFICATION_PAYMENT = 5;

    public static final String API_DATA_ACCEPT = "Accept";
    public static final String API_DATA_AUTHORIZATION = "Authorization";
    public static final String API_DATA_ACCEPT_APPLICATION_JSON = "application/json";
    public static final String APP_API_KEY = "APIKEY";



}
