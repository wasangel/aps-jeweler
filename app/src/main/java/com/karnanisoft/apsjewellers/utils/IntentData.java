package com.karnanisoft.apsjewellers.utils;

public class IntentData {
    public static final String KEY_VERIFY_OTP_SOURCE = "KEY_VERIFY_OTP_SOURCE";
    public static final int VERIFY_OTP_FORGOT_PASSWORD = 1002;
    public static final int VERIFY_OTP_REGISTRATION = 1001;
    public static final String KEY_MOBILE_NUMBER = "KEY_MOBILE_NUMBER";
    public static final String KEY_VERIFY_OTP = "KEY_VERIFY_OTP";
    public static final String KEY_PRODUCT_URL_ADDRESS = "KEY_PRODUCT_URL_ADDRESS";
    public static final String KEY_PRODUCT_LIST_TITLE = "KEY_PRODUCT_LIST_TITLE";
    public static final String USER_CURRENT_ADDRESS = "USER_CURRENT_ADDRESS";
    public static final int KEY_ADDRESS_ADDED = 1001;


    public static final String KEY_MAIN_ORDER_ID = "KEY_MAIN_ORDER_ID";
    public static final String KEY_COUPON_CODE = "KEY_COUPON_CODE";
    public static final String KEY_WEB_URL = "KEY_WEB_URL";
    public static final String KEY_WEB_TITLE = "KEY_WEB_TITLE";
    public static final String KEY_MAIN_ORDER_TEXT = "KEY_MAIN_ORDER_TEXT";
    public static final String KEY_PRODUCT_LIST_DESC = "KEY_PRODUCT_LIST_DESC";


    public static final String APP_JKS_PASSWORD = "buyouser";
    public static final String KEY_IMAGE_LIST = "KEY_IMAGE_LIST";
    public static final String KEY_PRODUCT_ID = "KEY_PRODUCT_ID";
}
